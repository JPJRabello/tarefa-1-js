let num1 = Math.round(Math.random()*100)+1;
let num2 = Math.round(Math.random()*100)+1;
let num3 = Math.round(Math.random()*100)+1;
let num4 = Math.round(Math.random()*100)+1;
let num5 = Math.round(Math.random()*100)+1;

let numeros = [num1, num2, num3, num4, num5];

for (let num of numeros){
    if (num%3 == 0 && num%5 != 0){
        console.log("Numero:",num,"Resultado: fizz");
    }
    if (num%5 == 0 && num%3 != 0){
        console.log("Numero:",num,"Resultado: buzz");
    }
    if (num%5 == 0 && num%3 == 0){
        console.log("Numero:",num, "Resultado: fizzbuzz");
    }
}